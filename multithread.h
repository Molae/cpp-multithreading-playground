//
// Created by Milton on 30-Apr-22.
//

#ifndef CPP_MULTITHREADING_PLAYGROUND_MULTITHREAD_H
#define CPP_MULTITHREADING_PLAYGROUND_MULTITHREAD_H

#include <thread>
#include <future>

namespace cppPlayground {
    class multithread {
    private:
        static void _sum(const std::vector<oddStruct> &vec, int &first, int &last, std::promise<int> &&p) {
            int sum = 0;
            for (int i = first; i < last; i++) {
                sum += vec[i].num;
            }
            p.set_value(sum);
        }

    public:
        static int sumVector(const std::vector<oddStruct>& vec) {
            auto processor_count = std::thread::hardware_concurrency();
            if (processor_count == 0) {
                processor_count = 1;
            }
            std::vector<std::thread> handles;
            std::vector<std::promise<int>> promises;
            std::vector<std::future<int>> futures;
            handles.reserve(processor_count);
            promises.reserve(processor_count);

            int currentAmountOfVectors = 0;

            for (int i = 0; i < processor_count; i++) {
                int first = currentAmountOfVectors;
                int last = currentAmountOfVectors + (int) vec.size() / (int) processor_count;
                currentAmountOfVectors = currentAmountOfVectors + (int) vec.size() / (int) processor_count;

                promises.emplace_back();
                futures.emplace_back(promises[i].get_future());
                handles.emplace_back(std::thread(_sum, std::ref(vec), std::ref(first), std::ref(last), std::move(promises[i])));
            }
            for (std::thread &h: handles) {
                h.join();
            }
            int finalSum = 0;

            for (auto &f: futures) {
                finalSum += f.get();
            }

            return finalSum;
        }
    };
}

#endif //CPP_MULTITHREADING_PLAYGROUND_MULTITHREAD_H
