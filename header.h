//
// Created by Milton on 30-Apr-22.
//

#ifndef CPP_MULTITHREADING_PLAYGROUND_HEADER_H
#define CPP_MULTITHREADING_PLAYGROUND_HEADER_H

#include <string>

class oddStruct {
public:
    std::string a{};
    size_t b{};
    int num{};
    bool TRUU{};
};

#endif //CPP_MULTITHREADING_PLAYGROUND_HEADER_H
