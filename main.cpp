#include <iostream>
#include "header.h"
#include "timer.h"
#include "multithread.h"

int test();

int main() {
    int sum = test();
    return 0;
}

int test() {
    std::vector<oddStruct> largeVector;
    int sum = 0;
    int sum2 = 0;
    for (int i = 0; i < 10000000; i++) {
        oddStruct s{};
        s.num = 1;
        largeVector.emplace_back(s);
    }

    {
        cppPlayground::timer a{};
        sum += cppPlayground::multithread::sumVector(largeVector);
    }
    {
        cppPlayground::timer b{};
        for (const oddStruct &i: largeVector) {
            sum2 += i.num;
        }
    }

    std::cout << sum << std::endl;
    std::cout << sum2 << std::endl;

    return sum;
}