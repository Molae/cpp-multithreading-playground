//
// Created by Milton on 30-Apr-22.
//

#ifndef CPP_MULTITHREADING_PLAYGROUND_TIMER_H
#define CPP_MULTITHREADING_PLAYGROUND_TIMER_H

#include <chrono>

namespace cppPlayground {
    class timer {
    private:
        std::chrono::time_point<std::chrono::high_resolution_clock> time;
    public:
        timer() {
            this->time = std::chrono::high_resolution_clock::now();
        }

        virtual ~timer() {
            std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::high_resolution_clock::now() - time) << std::endl;
        }
    };
}

#endif //CPP_MULTITHREADING_PLAYGROUND_TIMER_H
